namespace Core.Controllers {

    export class MainController {

        Title: string = 'Hello from Main Controller';

        static $inject: Array<string> = [];

        constructor() {
            this.Init();

        }

        private Init(): void {


        }



    }

    angular
        .module('Sample.App')
        .controller('MainController', Core.Controllers.MainController);
}






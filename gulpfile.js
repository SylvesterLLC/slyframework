var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var gulpConfiguration = require('./gulpconfig');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var minicss = require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');
var webserver = require('gulp-webserver');
var inject = require('gulp-inject');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var uglify = require('gulp-uglify');
var plumber = require('gulp-plumber');
let del = require('del');


gulp.task('less', function() {
    
    gulp.src(gulpConfiguration.Files.LessCompileFiles)
        .pipe(plumber({}))
        .pipe(sourcemaps.init())
        .pipe(less({}))
        .pipe(autoprefixer({
            browsers: ['> 5%']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(gulpConfiguration.Files.CSSRoot));   
});

gulp.task('webServerDev', function() {
    gulp.src(['./app/'])
        .pipe(webserver({
            livereload: {
                enable: true,
                port: 56138
            },
            path: '/',
            directoryListing: false,
            open: true,
            host: 'localhost',
            https: false,
            port: 5620,
            fallback: 'index.html'

        }));
});

gulp.task('dist', ["less"], function() {
    
    gulp.src([
        "./app/assets/css/slyframework.*",
        "./app/assets/css/**/*",
        "!./app/assets/css/custom*" 
            ])
        .pipe(gulp.dest("./dist/"));   
});

gulp.task('cleanBuild', [ 'dist'], () => {

    let promise = new Promise((resolve, reject) => {

        const DistDir = "./dist/";

        del([ DistDir  ])
            .then((x) => {
                console.log("Files Deleted");
                resolve();
            })
    });

    return promise;
});

gulp.task('watch', function() {

    gulp.watch([gulpConfiguration.Files.LessFiles, gulpConfiguration.Files.IndexFile], ['less', 'dist']);
    // gulp.watch([gulpConfiguration.Files.TypeScriptFiles], ['typescript']);
    

});

gulp.task('default', ['webServerDev', 'watch', 'less', 'dist', 'cleanBuild']); 
var AppRoot =  'app/';
var SrcRoot = 'src/';
var SrcAssetRoot = SrcRoot + 'assets/';
var BuildRoot = 'build/';
var AssetsRoot = AppRoot + 'assets/';


var IndexFileName = 'index.html';
var IndexFile = AppRoot + IndexFileName;

var LessRoot = SrcAssetRoot + 'less/';
var LessCompileFiles = LessRoot + '*.less';
var LessFiles = LessRoot + '**/*.less';

var CSSRoot = AssetsRoot  + 'css/';
var CSSFiles = CSSRoot  + '**/*.css';
var CSSMapRoot = CSSRoot;

var JavascriptAppRoot = AssetsRoot + 'js/';
var Angular_Modules = JavascriptAppRoot + '**/app.js';
var Angular_Controllers = JavascriptAppRoot + '**/Controller*/*Controller.js';

var AngularCustomFiles = [Angular_Modules, Angular_Controllers];

var TypeScriptRoot = SrcRoot + 'assets/ts/';
var TypeScriptAppRoot =  JavascriptAppRoot;
var TypeScriptFiles = TypeScriptRoot + '**/*.ts';

var BabelScriptRoot = SrcRoot + 'assets/babel/';
var BabelFiles = BabelScriptRoot + '**/*.js';

var HtmlFiles = AppRoot + '**/*.html'


var config = {
AppRoot: AppRoot,
SrcRoot: SrcRoot,
BuildRoot: BuildRoot,
AssetsRoot: AssetsRoot,

IndexFileName: IndexFileName,
IndexFile: IndexFile,

JavascriptAppRoot: JavascriptAppRoot,

AngularCustomFiles: AngularCustomFiles,
Angular_Modules: Angular_Modules, 
Angular_Controllers: Angular_Controllers,

LessRoot: LessRoot,
LessCompileFiles: LessCompileFiles,
LessFiles: LessFiles,

CSSRoot: CSSRoot,
CSSMapRoot: CSSMapRoot,
CSSFiles: CSSFiles,

TypeScriptRoot: TypeScriptRoot,
TypeScriptAppRoot:  TypeScriptAppRoot,
TypeScriptFiles: TypeScriptFiles,

BabelScriptRoot: BabelScriptRoot,
BabelFiles: BabelFiles,

HtmlFiles: HtmlFiles

};


module.exports = {
    Files: config
};
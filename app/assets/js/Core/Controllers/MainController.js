var Core;
(function (Core) {
    var Controllers;
    (function (Controllers) {
        var MainController = (function () {
            function MainController() {
                this.Title = 'Hello from Main Controller';
                this.Init();
            }
            MainController.prototype.Init = function () {
            };
            MainController.$inject = [];
            return MainController;
        }());
        Controllers.MainController = MainController;
        angular
            .module('Sample.App')
            .controller('MainController', Core.Controllers.MainController);
    })(Controllers = Core.Controllers || (Core.Controllers = {}));
})(Core || (Core = {}));
